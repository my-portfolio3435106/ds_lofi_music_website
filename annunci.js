fetch('./annunci.json').then( (response)=> response.json() ).then( (data)=> {
    
    let categoryWrapper = document.querySelector('#categoryWrapper');
    
    let cardsWrapper = document.querySelector('#cardsWrapper');
    
    function setCategoryFilters(){
        let categories = data.map( (annuncio)=> annuncio.category);
        
        let uniqueCategories = [];
        
        categories.forEach( (category)=> {
            if( !uniqueCategories.includes(category) ){
                uniqueCategories.push(category);
            }
        })
        
        uniqueCategories.forEach( (category)=> {
            let div = document.createElement('div');
            div.classList.add('form-check');
            div.innerHTML = `
            
            <input class="form-check-input" type="radio" name="flexRadioDefault" id="${category}">
            <label class="form-check-label" for="${category}">
            ${category}
            </label>
            
            `
            categoryWrapper.appendChild(div);
            
            
        })
        
        
        
    }
    
    setCategoryFilters();
    
    
    function showCards(array){
        
        cardsWrapper.innerHTML = '';
        
        array.sort( (a, b)=> +a.price - +b.price )
        
        array.forEach( (element)=> {
            let div = document.createElement('div');
            div.classList.add('announcement-card');
            div.innerHTML = `
            
            <p class="h3">${element.name}</p>
            <p class="h4">${element.category}</p>
            <p class="lead fw-bold">${element.price}€</p>
            `
            cardsWrapper.appendChild(div);
        })
        
    }
    
    showCards(data);
    
    let checkInputs = document.querySelectorAll('.form-check-input');
    
    
    function filterByCategory(array){
        
        // Per poter trovare il bottone con l'attributo checked dovremo utilizzare il metodo .find() degli array. Il problema e' che checkInputs non e' un array bensi' un array-like. Dovremo quindi trasformare la nostra NodeList in un array attraverso un metodo dell'oggetto Array. Questo metodo si chiama .from()
        
        // let arrayFromNodeList = Array.from(checkInputs);
        // let button = arrayFromNodeList.find( (bottone)=> bottone.checked );
        // let categoria = button.id;
        
        let categoria = Array.from(checkInputs).find( (bottone)=> bottone.checked ).id;
        
        if(categoria != 'All'){
            let filtered = array.filter( (annuncio)=> annuncio.category == categoria );
            return filtered;
        } else {
            return data;
        }
    }
    
    
    
    
    let priceInput = document.querySelector('#priceInput');
    let incrementNumber = document.querySelector('#incrementNumber');
    
    function setPriceInput(){
        
        let prices = data.map( (annuncio)=> +annuncio.price );
        let maxPrice = Math.max(...prices);
        priceInput.max = Math.ceil(maxPrice);
        priceInput.value = Math.ceil(maxPrice);
        incrementNumber.innerHTML = Math.ceil(maxPrice);
    }
    
    setPriceInput()
    
    
    function filterByPrice(array){
        let filtered = array.filter( (annuncio)=> +annuncio.price <= +priceInput.value );
        return filtered;
    }
    
    
    
    let wordInput = document.querySelector('#wordInput');
    
    function filterByWord(array){
        
        let nome = wordInput.value;
        
        if(nome.length > 1){
            let filtered = array.filter( (annuncio)=> annuncio.name.toLowerCase().includes(nome.toLowerCase()) );
            return filtered;
        } else {
            let filtered = array.filter( (annuncio)=> annuncio.name[0].toLowerCase().includes(nome.toLowerCase()) );
            return filtered;
            
        }
        
    }
    
    
    // La funzione che dovra' gestire tutti e tre i filtri sara' globalFilter(). Questa funzione lanca tutti e tre i filtri in sequenza, ciascun filtro agisce sul filtro precedente tranne il primo che agisce su data.
    
    function globalFilter(){
        
        let filteredByCategory = filterByCategory(data);
        let filteredByPrice = filterByPrice(filteredByCategory);
        let filteredByWord = filterByWord(filteredByPrice);
        
        showCards(filteredByWord);
    }
    
    
    
    checkInputs.forEach( (categoryInput)=> {
        categoryInput.addEventListener( 'click' , ()=> {
            globalFilter();
        } )
    })
    
    
    priceInput.addEventListener( 'input', ()=>{
        incrementNumber.innerHTML = priceInput.value;
        globalFilter();
    } )
    
    
    wordInput.addEventListener('input' , ()=>{
        globalFilter();
    })
    
})
