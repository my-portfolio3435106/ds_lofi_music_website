let logo = document.querySelector('#logo');
let links = document.querySelectorAll('.nav-link');

console.dir(logo);

window.addEventListener('scroll' , ()=>{
    let scrolled = window.scrollY;
    if(scrolled > 0){
        logo.classList.remove('logo0');
        logo.classList.add('logo1');
        links.forEach((link)=>{
            link.classList.add('d-none');
        })
    }else{
        logo.classList.remove('logo1');
        logo.classList.add('logo0');
        links.forEach((link)=>{
            link.style.color = 'var(--cute)';
            link.classList.remove('d-none')
        })
}})

let firstNumber = document.querySelector('#firstNumber');
let secondNumber = document.querySelector('#secondNumber');
let thirdNumber = document.querySelector('#thirdNumber');


function createInterval(range, speed, element){
    
    let counter = 0;
    
    let interval = setInterval(() => {
        
        if( counter < range ){
            counter++;
            element.innerHTML = counter;
        }else{
            clearInterval(interval);
        }
        
    }, speed);
}



// IntersectionObserver

let test = document.querySelector('#test');
let confirm = false;

let observer = new IntersectionObserver(
    (entries)=>{
        
        entries.forEach((entry)=>{
            if(entry.isIntersecting && confirm == false){
                confirm = true;
                createInterval(980, 1, firstNumber);
                createInterval(200, 10, secondNumber);
                createInterval(2000, 1, thirdNumber);
                
            } 
        })
        
    }
    
    );
    
    observer.observe(test);

    let announcementsWrapper = document.querySelector('#announcementsWrapper');

    let announcements = [
        {name: 'Lofi v21', category: 'Lofi Chill', duration: '4 minuti', image: './media/y-tt-huli2.jpg'},
        {name: 'Lofi v22', category: 'Lofi Pomo', duration: '35 minuti', image: './media/y-tt-huli2.jpg'},
        {name: 'Lofi v23', category: 'Lofi Vibe', duration: '3.20 minuti', image: './media/y-tt-huli2.jpg'},
        {name: 'Lofi v24', category: 'Lofi Pomo', duration: '42 minuti', image: './media/y-tt- (1).jpg'},
        {name: 'Lofi v25', category: 'Lofi Chill', duration: '5 minuti', image: './media/y-tt-huli2.jpg'},
        {name: 'Lofi v26', category: 'Lofi Vibe', duration: '4 minuti', image: './media/Ytt.png'},
    ]

    announcements.forEach((announcement, i)=>{
        
        if(i >= (announcements.length - 3)){
            let div = document.createElement('div');
            div.classList.add('col-2', 'cubo-card',);
            div.innerHTML = `
            
            <div class="card text-cute img-big" style="width: 15rem;" data-aos="fade-right" data-aos-duration="2000">
                <img src="${announcement.image}" class="card-img-top img-fluid " alt="...">
            <div class="card-body ">
              <h2 class="card-title">${announcement.name}</h2>
              <h5 class="card-text">${announcement.category}</h5>
              <p class="card-text">${announcement.duration}</p>
            </div>
            </div>
        
            `
            announcementsWrapper.appendChild(div);
        }
        
    })

    let cubo = document.querySelector('.cube');
    let cuboCard = document.querySelectorAll('.cubo-card');
    cubo.addEventListener('click', ()=>{
        cuboCard.forEach(element => {
            element.classList.toggle('d-none');
        })
        announcementsWrapper.classList.toggle('justify-content-center')
        announcementsWrapper.classList.toggle('custom-margin')
        }
       
    )
    
    let swiperWrapper = document.querySelector('#swiper-wrapper1');

let advices = [
    {name: 'Karl', description: 'Beats for your Heart', rank: 5},
    {name: 'Erebeth', description: 'Divine', rank: 4},
    {name: 'Sasha', description: `Not for Me`, rank: 1},
    {name: 'Nineveh', description: 'Uwu', rank: 3},
    {name: 'Nia', description: `Not Bad`, rank: 2},
    {name: 'Kadan', description: 'Pls More!!!', rank: 5},
]

advices.forEach((advice)=>{
    let div = document.createElement('div');
    div.classList.add('swiper-slide');
    div.innerHTML = `
    
    <div class="advice-card card text-center">
        <p class="h2 text-w">${advice.name}</p>
        <p class="lead text-cute">${advice.description}</p>
        <div class="star-wrapper">
           
        </div>
    </div>
    
    `

    swiperWrapper.appendChild(div);

})

let starWrappers = document.querySelectorAll('.star-wrapper');


starWrappers.forEach((wrapper, index)=>{

    let voto = advices[index].rank;

    for(let i = 1; i <= voto; i++){
        let icon = document.createElement('i');
        icon.classList.add('fa-solid', 'fa-star', 'text-w');
        wrapper.appendChild(icon);
    }

    if(voto < 5){
        let resto = 5 - voto;
        for(let i = 1; i <= resto; i++){
            let icon = document.createElement('i');
            icon.classList.add('fa-regular', 'fa-star', 'text-w');
            wrapper.appendChild(icon);
        }
    }

})

fetch('./annunci.json').then( (response)=> response.json() ).then( (data)=> {
    
    let categoryWrapper = document.querySelector('#categoryWrapper');
    
    let cardsWrapper = document.querySelector('#cardsWrapper');
    
    function setCategoryFilters(){
        let categories = data.map( (annuncio)=> annuncio.category);
        
        let uniqueCategories = [];
        
        categories.forEach( (category)=> {
            if( !uniqueCategories.includes(category) ){
                uniqueCategories.push(category);
            }
        })
        
        uniqueCategories.forEach( (category)=> {
            let div = document.createElement('div');
            div.classList.add('form-check');
            div.innerHTML = `
            
            <input class="form-check-input" type="radio" name="flexRadioDefault" id="${category}">
            <label class="form-check-label" for="${category}">
            ${category}
            </label>
            
            `
            categoryWrapper.appendChild(div);
            
            
        })
        
        
        
    }
    
    setCategoryFilters();

    function showCards(array){
        
        cardsWrapper.innerHTML = '';
        
        array.sort( (a, b)=> +a.price - +b.price )
        
        array.forEach( (element)=> {
            let div = document.createElement('div');
            div.classList.add('announcement-card');
            div.innerHTML = `
            
            <p class="h3">${element.name}</p>
            <p class="h4">${element.category}</p>
            <p class="lead fw-bold">${element.price}€</p>
            `
            cardsWrapper.appendChild(div);
        })
        
    }
    
    showCards(data);

    let checkInputs = document.querySelectorAll('.form-check-input');
    
    
    function filterByCategory(array){
        
        // Per poter trovare il bottone con l'attributo checked dovremo utilizzare il metodo .find() degli array. Il problema e' che checkInputs non e' un array bensi' un array-like. Dovremo quindi trasformare la nostra NodeList in un array attraverso un metodo dell'oggetto Array. Questo metodo si chiama .from()
        
        // let arrayFromNodeList = Array.from(checkInputs);
        // let button = arrayFromNodeList.find( (bottone)=> bottone.checked );
        // let categoria = button.id;
        
        let categoria = Array.from(checkInputs).find( (bottone)=> bottone.checked ).id;
        
        if(categoria != 'All'){
            let filtered = array.filter( (annuncio)=> annuncio.category == categoria );
            return filtered;
        } else {
            return data;
        }
    }
    
    
    
    
    let priceInput = document.querySelector('#priceInput');
    let incrementNumber = document.querySelector('#incrementNumber');
    
    function setPriceInput(){
        
        let prices = data.map( (annuncio)=> +annuncio.price );
        let maxPrice = Math.max(...prices);
        priceInput.max = Math.ceil(maxPrice);
        priceInput.value = Math.ceil(maxPrice);
        incrementNumber.innerHTML = Math.ceil(maxPrice);
    }
    
    setPriceInput()
    
    
    function filterByPrice(array){
        let filtered = array.filter( (annuncio)=> +annuncio.price <= +priceInput.value );
        return filtered;
    }
    
    
    
    let wordInput = document.querySelector('#wordInput');
    
    function filterByWord(array){
        
        let nome = wordInput.value;
        
        if(nome.length > 1){
            let filtered = array.filter( (annuncio)=> annuncio.name.toLowerCase().includes(nome.toLowerCase()) );
            return filtered;
        } else {
            let filtered = array.filter( (annuncio)=> annuncio.name[0].toLowerCase().includes(nome.toLowerCase()) );
            return filtered;
            
        }
        
    }
    
    
    // La funzione che dovra' gestire tutti e tre i filtri sara' globalFilter(). Questa funzione lanca tutti e tre i filtri in sequenza, ciascun filtro agisce sul filtro precedente tranne il primo che agisce su data.
    
    function globalFilter(){
        
        let filteredByCategory = filterByCategory(data);
        let filteredByPrice = filterByPrice(filteredByCategory);
        let filteredByWord = filterByWord(filteredByPrice);
        
        showCards(filteredByWord);
    }
    
    
    
    checkInputs.forEach( (categoryInput)=> {
        categoryInput.addEventListener( 'click' , ()=> {
            globalFilter();
        } )
    })
    
    
    priceInput.addEventListener( 'input', ()=>{
        incrementNumber.innerHTML = priceInput.value;
        globalFilter();
    } )
    
    
    wordInput.addEventListener('input' , ()=>{
        globalFilter();
    })
    
})

